from rest_framework import viewsets,mixins,filters,generics
from django_filters.rest_framework import DjangoFilterBackend
from .serializers import ProductSerializer
from shop.models import Product

from rest_framework import filters
# class ProductViewset(mixins.ListModelMixin,
#                     mixins.RetrieveModelMixin,
#                     viewsets.GenericViewSet,):

#     queryset            = Product.objects.all()
#     serializer_class    = ProductSerializer
#     filter_backends     = [filters.SearchFilter,]
#     filter_fields       = ['name','description']

class ProductViewset(viewsets.ModelViewSet):

    queryset            = Product.objects.all().distinct('name')
    serializer_class    = ProductSerializer
    filter_backends     = [filters.SearchFilter,]
    search_fields       = ['name','description']