getPrice = function() {
  var numVal1 = Number(document.getElementById("price").value);
  var numVal2 = 105 / 100;
  var totalValue = (numVal1 * numVal2)
  document.getElementById("total").value = totalValue.toFixed(2);
}
getDiscount = function() {
  var numVal1 = Number(document.getElementById("price").value);
  var numVal2 = Number(document.getElementById("discount").value)/100;
  var total = numVal1 + (numVal1 * numVal2)
  document.getElementById("sale").value = total.toFixed(2);
}