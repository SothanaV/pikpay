search().then(product => renderSuggestionList(product))
function search() {
    let searchText = document.querySelector('#searchbox').value

    return axios.get(`/api/v1/product/?format=json&search=${searchText}`)
    .then(response => response.data)
    .catch(error => console.error(error))
}

function onKeyupHandler(e) {
    if (e.keyCode === 13) {
        search().then(product => renderListedPeople(product))
    } 
    else {
        search().then(product => renderSuggestionList(product))
    }
}
    function searchBySuggestion(name, last_name) {
    document.querySelector('#searchbox').value = `${name} ${last_name}`

    search().then(people => renderListedPeople(people))
}
function renderSuggestionList(product) {
    console.log(product)
    let renderSuggestionList = ''

    if (product.results.length != 0) {
        product.results.forEach(function(p){
            renderSuggestionList += `
                        <li>
                            <a href="/search/?search=${p.name}">
                                ${p.name}
                            </a>
                        </li>
                    `
        });
    }
    else {
    renderSuggestionList = '<li>ไม่มีคำแนะนำ</li>'
    }

    let suggestionDOM = document.querySelector('.suggestions')
    suggestionDOM.innerHTML = renderSuggestionList
}
function getBuck(){
    return axios.get(`/shop/api/mybuck/`).
    then(response => response.data)
    .catch(error => console.error(error))
}
function bucket(){
    getBuck().then(data=>renderBuck(data))
}
function renderBuck(data){
    console.log(data)
    var renderText = ''
    var count=0
    var total_price =0
    if(data.length!=0){
        data.forEach(d=>{
            // console.log(d);
            renderText += `<li>
                    <div class="uk-child-width-expand@s uk-margin-remove" uk-grid> 
                        <div class="uk-padding-remove " id="img1"><img src="${d.image}"></div>
                        <div class="uk-padding-remove uk-display-block"> 
                            <h1>${d.name}</h1><br>
                            <div class="uk-display-inline">
                                <p> ${d.amount} </p>
                                
                            </div>
                        </div>
                    </div>
                    <div class="uk-text-right" >
                        <a onclick="del_buck(${d.id})">remove</a>
                        <br>
                        <h2>฿ ${d.price}</h2>
                    </div>
                </li>`
            
            count+=1
            total_price+=d.price
        })
    }
    document.getElementById('cart_item').innerHTML = renderText;
    document.getElementById('count').innerHTML = count
    document.getElementById('total_price').innerHTML = total_price
}
function del_buck(pk){
    axios.get(`/del_bucket/${pk}`).
    then(response => response)
    .catch(error => console.error(error))
    bucket()
}