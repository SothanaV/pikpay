function render_star(data){
    var pk = data['pk']
    var star = data['star']
    var text = "";
    
    for (var x=0 ; x < 5 ; x++) {
        if(x<star) {
            text += '<i class="lar la-star starchecked"></i>';
        }
        else {
            text += '<i class="lar la-star star"></i>';
        }                    
    }
    document.getElementById(`star${pk}`).innerHTML = text
}