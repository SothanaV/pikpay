from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_in
from django.shortcuts  import redirect
# Create your models here.

class Account(models.Model):
    user = models.OneToOneField(User,on_delete=models.PROTECT)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    dob = models.DateField(auto_now=False, auto_now_add=False,null=True)
    image = models.ImageField(null=True)
    GENDER_CHOICE = [
        ('m','male'),
        ('f','female'),
        ('r','Rather not say')
    ]
    gender = models.CharField(max_length=1,choices =GENDER_CHOICE)
    def __str__(self):
        return "{}-{}_{}".format(self.user,self.first_name,self.last_name)

class Provinces(models.Model):
    name_th = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100)
    def __str__(self):
        return "{}/{}".format(self.name_th,self.name_en)
    
class Amphurs(models.Model):
    amphur_id = models.PositiveSmallIntegerField()
    province = models.ForeignKey(Provinces,on_delete=models.CASCADE)
    name_th = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100)

class Districts(models.Model):
    amphur = models.ForeignKey(Amphurs,on_delete=models.CASCADE)
    name_th = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100)
    zipcode = models.CharField(max_length=5)

    def __str__(self):
        return "{} {} {} {}".format(self.name_th,self.amphur.name_th,self.amphur.province.name_th,self.zipcode)
    

class Address(models.Model):
    name = models.CharField(max_length=200)
    phone = models.CharField(max_length=20)
    address1 = models.TextField()
    address2 = models.ForeignKey(Districts,on_delete=models.CASCADE)
    account = models.ForeignKey(Account,on_delete=models.CASCADE)
    is_use = models.BooleanField(default=False)

    def getAddr(self):
        return "{} {} | {} {} {} {} {}".format(
                self.name,
                self.phone,
                self.address1,
                self.address2.name_th,
                self.address2.amphur.name_th,
                self.address2.amphur.province.name_th,
                self.address2.zipcode)
def logged_in_handle(sender, user, request, **kwargs):
    index = User.objects.filter(username=request.user)[0]
    profile = Account.objects.filter(user=request.user)
    if not profile.exists():
        print("not have profile")
        create_init_profile = Account.objects.update_or_create(
            user = index,
            first_name = index.first_name,
            last_name = index.last_name,
        )
user_logged_in.connect(logged_in_handle)
