from django.contrib import admin
from .models import Account,Address,Amphurs,Districts,Provinces
# Register your models here.

admin.site.register(Account)
admin.site.register(Address)

class AmphurssAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Amphurs._meta.fields]
admin.site.register(Amphurs,AmphurssAdmin)

class DistrictsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Districts._meta.fields]
admin.site.register(Districts,DistrictsAdmin)

class ProvincesAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Provinces._meta.fields]
admin.site.register(Provinces,ProvincesAdmin)
