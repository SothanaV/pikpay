from django.urls import path
from .views import login,home,logout,register,zipcode,amphur

app_name = 'authen'
urlpatterns = [
    path('login/',login,name='login'),
    path('logout/',logout,name='logout'),
    path('home/',home,name='home'),
    path('register/',register,name='register'),

    # API #
    path('zipcode/',zipcode),
    path('amphur/',amphur),
]
