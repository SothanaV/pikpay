from django.shortcuts import render,redirect,reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, logout as signout, login as auth_login
from django.contrib.auth.models import User
from .models import Districts,Amphurs
from django.http import JsonResponse
# Create your views here.
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request,user,backend='django.contrib.auth.backends.ModelBackend')
            return redirect('shop:home')
        else:
            context = {'msg':'Invalid Username or Password'}
            return render(request, 'authen/login.html',context)
    return render(request, 'authen/login.html')

def logout(request):
    signout(request)
    return redirect(reverse('shop:home'))

def register(request):
    if request.method == 'POST':
        username    = request.POST['registeredUsername']
        password    = request.POST['registeredPassword']
        db_user     = User.objects.filter(username=username)
        if not db_user.exists(): 
            print("don't have user")
            user = User.objects.create_user(
                username = username,
                password = password,
            )
            user = authenticate(username=username, password=password)
            auth_login(request, user)
            return redirect('shop:home')
        else:
            # return redirect('shop:home')
            context = {'msg':'Username have exists please use other'}
            return render(request, 'authen/login.html',context)  
    else:
        return render(request, 'authen/login.html')

@login_required
def home(request):
    return render(request, 'authen/home.html')

def zipcode(request):
    district = list(Districts.objects.filter(zipcode=request.GET.get('zipcode')).values('name_en','name_th','amphur','pk'))
    return JsonResponse(district,safe=False)

def amphur(request):
    amphur = list(Districts.objects.filter(pk=int(request.GET.get('amphur'))).values('amphur__name_en','amphur__name_th','amphur__province__name_th','amphur__province__name_en'))
    return JsonResponse(amphur,safe=False)