from django.shortcuts               import render,redirect,HttpResponse
from django.http                    import JsonResponse
from django.contrib.auth.decorators import login_required
from django.core.paginator          import Paginator, EmptyPage, PageNotAnInteger
from django.db.models               import Count,Sum,Avg

from .models        import Product, Category, Detail, Bucket, Buyer, CreaditCard, \
                            Transaction, Favorite, FlashSale,Logistic,Seller,Bank, \
                            Review, Picture
from authen.models  import Account,Address,Districts
from django.views.decorators.csrf import csrf_exempt

import calendar
import datetime
import random
ITEMS_PER_PAGE = 18 

# Create your views here.
def home(request):
    # Querry
    try:
        user = Account.objects.get(user=request.user)
    except Exception:
        pass
    else:
        if len(user.first_name)<=0 and len(user.phone)<=0:
            return redirect('shop:editprofile')
    best_sale = Product.objects.filter(is_use=True).order_by('-sales')[:4]
    flash_sale = FlashSale.objects.filter(date=datetime.datetime.now())
    try:
        favourite = Favorite.objects.filter(user__account__user=request.user,is_use=True).values('product__pk','product__category__pk')
    except Exception as e:
        print(e)
        hot_promotion = best_sale
        justforyou = best_sale
    else:
        product_pk = [x['product__pk'] for x in favourite]
        hot_product = Product.objects.filter(pk__in=product_pk,is_use=True).order_by('-sales')
        hot_promotion = hot_product | best_sale

        category_pk = [x['product__category__pk'] for x in favourite]
        justforyou = Product.objects.filter(category__pk__in=category_pk,is_use=True)[:ITEMS_PER_PAGE]
    try:
        context = {'first':best_sale[0],
                    'second':best_sale[1],
                    'best_sale':best_sale[2:],
                    'third':best_sale[3],
                    'hot_promotion':hot_promotion[:10],
                    'hot_promotion_mobile':hot_promotion[:3],
                    'flash_sale':flash_sale,
                    'justforyou':justforyou,
                    'today':datetime.datetime.now().strftime("%Y-%m-%dT23:59:59+07:00"),
                }
    except Exception as e:
         context = {}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/Home.html',context)
    else:
        return render(request,'shop/web/Home.html',context)

def homeandlifestyle(request):
    category = Category.objects.filter(name='homeandlifestyle')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}

    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_lifestyle.html',context)
    else:
        return render(request,'shop/web/HomeAndLifestyle.html',context)

def fashion(request):
    category = Category.objects.filter(name='fashion')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_fashion.html',context)
    else:
        return render(request,'shop/web/Fashion.html',context)

def healthandbeauty(request):
    category = Category.objects.filter(name='healthandbeauty')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_beauty.html',context)
    else:
        return render(request,'shop/web/HealthAndBeauty.html',context)

def sportsandtravel(request):
    category = Category.objects.filter(name='sportsandtravel')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_sport.html',context)
    else:
        return render(request,'shop/web/SportsAndTravel.html',context)

def groceriesandpets(request):
    category = Category.objects.filter(name='groceriesandpets')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_pet.html',context)
    else:
        return render(request,'shop/web/GroceriesAndPets.html',context)

def toys(request):
    category = Category.objects.filter(name='toys')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_toy.html',context)
    else:
        return render(request,'shop/web/Toys.html',context)

def electronics(request):
    category = Category.objects.filter(name='electronics')[0]
    product = Product.objects.filter(category=category,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_electronic.html',context)
    else:
        return render(request,'shop/web/Electronics.html',context)

def justforyou(request):
    # category = Category.objects.filter(name='electronics')[0]
    # product = Product.objects.filter(category=category)
    '''wait for just for you algorithm'''
    product = Product.objects.filter(is_use=True)
    page = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/page_justforu.html',context)
    else:
        return render(request,'shop/web/JustForYou.html',context)
        
@login_required
def profile(request):
    profile = Account.objects.get(user=request.user)
    now_year = datetime.datetime.now().year
    address = Address.objects.filter(account=profile).order_by('-is_use')
    creadit_card = CreaditCard.objects.filter(account=profile)
    context = {'profile':profile,
                'address':address,
                'creadit_card':creadit_card,
                'month':["%02d"%m for m in range(1,13)],
                'year':["%02d"%y for y in range(now_year+10)],
            }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/profile.html',context)
    else:
        return render(request,'shop/web/Profile.html',context)

@login_required
def editprofile(request):
    if request.method == 'POST':
        account = Account.objects.get(user=request.user)
        data = request.POST
        name = data['name'].split(' ')
        account.first_name  = name[0]
        account.last_name   = name[-1]
        account.phone       = data['phone']
        account.gender      = data['gender']
        dob = "{}-{}-{}".format(data['year'],data['month'],data['day'])
        account.dob = datetime.datetime.strptime(dob,"%Y-%B-%d").date()
        account.save()
        return redirect('shop:profile')
    profile  = Account.objects.get(user=request.user)
    now_year = datetime.datetime.now().year
    context  = {'profile':profile,
                'day':[i+1 for i in range(31)],
                'month':[(i,calendar.month_name[i]) for i in range(1,13)],
                'year':[i for i in range(1900,now_year+1)]}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/editProfile.html',context)
    else:
        return render(request,'shop/web/EditProfile.html',context)

@login_required
def shippingaddress(request):
    profile = Account.objects.get(user=request.user)
    if request.method == 'POST':
        data = request.POST
        use  = False
        if 'seva_as_default' in data:
            if data['seva_as_default'] == 'on':
                use = True
                for x in Address.objects.filter(account=profile):
                    x.is_use = False
                    x.save()
            Address.objects.all().is_use = False
        Address.objects.create(
            name    = data['name'],
            phone   = data['phone'],
            address1 = data['address'],
            address2 = Districts.objects.get(pk=int(data['district'])),
            account = Account.objects.get(user=request.user),
            is_use  = use,
        )
    address = Address.objects.filter(account=profile).order_by('-is_use')
    context = {'profile':profile,'address':address}
    if request.user_agent.is_mobile:
        return redirect('shop:profile')
    else:
        return render(request,'shop/web/Shipping_Address.html',context)

@login_required
def del_shippingaddress(request,pk,page):
    Address.objects.get(pk=pk).delete()
    if page == "edit":
        return redirect('shop:editaddress')
    else:
        return redirect('shop:shippingaddress')

@login_required
def set_dault_shippingaddress(request,pk,page):
    for x in Address.objects.filter(account=Account.objects.get(user=request.user)):
        x.is_use = False
        x.save()
    x = Address.objects.get(pk=pk)
    x.is_use = True
    x.save()
    if page == "edit":
        return redirect('shop:editaddress')
    else:
        return redirect('shop:shippingaddress')

@login_required
def product(request,pk):
    product = Product.objects.get(pk=pk)
    details = Detail.objects.filter(product=product)

    '''product count'''
    num_product = Product.objects.filter(seller=product.seller,is_use=True).aggregate(Count('pk'))
    fav = Favorite.objects.filter(user__account__user=request.user,product=product)
    if fav.exists():
        is_fav = fav[0].is_use
    else:
        is_fav = False
    sold_of_shop = Transaction.objects.filter(product__product__seller=product.seller).aggregate(Count('pk'))
    stock = Detail.objects.filter(product=product).aggregate(Sum('amount'))
    ship_form = Address.objects.filter(account=product.seller.account)
    if ship_form.exists():
        ship_form = ship_form[0].address2.amphur.name_en + "," + ship_form[0].address2.amphur.province.name_en
    else:
        ship_form = 'Some where'
    reviews = Review.objects.filter(product=product)
    rating = Review.objects.filter(product__seller=product.seller).aggregate(Avg('star'))
    try:
        rating = "%2.2f"%rating['star__avg']
    except Exception as e:
        print(e)
        rating = 0
    context = {'product':product,
                'details':details,
                'num_product':num_product['pk__count'],
                'is_fav':is_fav,
                'sold_of_shop':sold_of_shop['pk__count'],
                'stock':stock['amount__sum'],
                'ship_form': ship_form,
                'reviews':reviews,
                'rating':rating,
            }
    if request.method=='POST':
        data = request.POST
        if data['model'] != '':
            quantity = int(data['quantity'])
            detail = Detail.objects.get(pk=data['model'])
            if quantity <= detail.amount:
                Bucket.objects.create(
                    buyer   = Buyer.objects.get(account__user=request.user),
                    product = detail,
                    amount  = quantity
                )
                detail.amount -= quantity
                detail.save()
                if 'buy' in data:
                    return redirect('shop:mycart')
                context['msg2'] = "Product add to mycart"
            else:
                context['msg'] = "{} model has {} ".format(detail.model,detail.amount)
        else:
            context['msg'] = "Please select model" 
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/product.html',context)
    else:
        return render(request,'shop/web/product.html',context)

@login_required
def creditcard(request):
    account = Account.objects.get(user=request.user)
    creadit_card = CreaditCard.objects.filter(account=account)
    now_year = int(datetime.datetime.now().strftime("%y"))
    context = {'creadit_card':creadit_card,
                'month':["%02d"%m for m in range(1,13)],
                'year':["%02d"%y for y in range(now_year+10)],
                'profile':account
    }
    if request.method == 'POST':
        data = request.POST
        CreaditCard.objects.create(
            account     = account,
            card_number = data['card_no'],
            hold_name   = data['name'],
            expire  = datetime.datetime.strptime("{}-{}".format(data['month'],data['year']),"%m-%y").date(),
            secure  = data['secure'],
            kind    = CreaditCard.creadit_card_type(data['card_no']),
        )
    if request.user_agent.is_mobile:
        return redirect('shop:profile')
    else:
        return render(request,'shop/web/Credit_Card.html',context)

@login_required
def delete_card(request,pk):
    CreaditCard.objects.get(pk=pk).delete()
    return redirect('shop:creditcard')

@login_required
def order(request):
    profile = Account.objects.get(user=request.user)
    all_order = Transaction.objects.filter(buyer__account=profile).exclude(status='cc').order_by('-id')
    context = {'profile':profile,
                'all_order':all_order,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/order.html',context)
    else:
        return render(request,'shop/web/Order.html',context)

@login_required
def myfavourite(request):
    profile = Account.objects.get(user=request.user)
    favorite = Favorite.objects.filter(user__account=profile,is_use=True)
    context = {'profile':profile,'favorite':favorite}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/favorite.html',context)
    else:
        return render(request,'shop/web/MyFavourite.html',context)

@login_required
def add_favorite(request,pk):
    account = Buyer.objects.get(account__user=request.user)
    product = Product.objects.get(pk=pk)
    favorite = Favorite.objects.filter(user=account,product=product)
    if not favorite.exists():
        try:
            Favorite.objects.create(
                user = account,
                product = product,
                is_use = True
            )
        except Exception as e:
            print(e)
            return HttpResponse(status=400)
    else:
        fav = favorite[0]
        if fav.is_use == False:
            fav.is_use = True
        else:
            fav.is_use = False
        fav.save()
        return HttpResponse(status=200)
    return HttpResponse(status=201)

@login_required
def reviewproduct(request,pk):
    profile = Account.objects.get(user=request.user)
    order = Transaction.objects.get(pk=pk,buyer__account=profile)
    if request.method == 'POST':
        data = request.POST
        print(data)
        if 'rating3' in data:
            star = int(data['rating3'])
        else:
            star = 5
        review = Review.objects.create(
            transaction = order,
            buyer = Buyer.objects.get(account=profile),
            product = order.product.product,
            star = star,
            description = data['comment'],
        )
        try:
            photo = [int(pic) for pic in data['picture'].split(',')]
        except:
            photo = []
        if len(photo)>0:
            for pic in photo:
                review.photo.add(pic)
        order.status = 'ss'
        order.save()
    review = Review.objects.filter(product=order.product.product,buyer__account=profile,transaction=order)
    print(review)
    reviewed = review.exists()
    if reviewed:
        review = review[0]
    context = {
        'profile':profile,
        'order':order,
        'reviewed':reviewed,
        'review':review
    }

    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/review.html',context)
    else:
        return render(request,'shop/web/Review_product.html',context)

@login_required
def review(request):
    profile = Account.objects.get(user=request.user)
    transaction = Transaction.objects
    wait_review = transaction.filter(buyer__account__user=request.user,status='re')
    reviewed = transaction.filter(buyer__account__user=request.user,status='ss')
    review_all = wait_review | reviewed
    context = {
        'profile':profile,
        'wait_review':wait_review,
        'reviewed':reviewed,
        'review_all':review_all,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/review.html',context)
    else:
        return render(request,'shop/web/Review.html',context)

@login_required
def mycart(request):
    bucket = Bucket.objects.filter(buyer__account__user=request.user,is_use=True)
    amount = bucket.aggregate(Sum('amount'))
    total_price = 0
    for item in bucket:
        total_price+= item.amount * item.product.product.price
    context = {'bucket':bucket,
                'total_amount':amount['amount__sum'],
                'total_price':total_price
            }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/myCart.html',context)
    else:
        return render(request,'shop/web/Mycart.html',context)

def search(request):
    search_key = request.GET.get('search','')
    product = Product.objects.filter(name=search_key,is_use=True)
    page    = request.GET.get('page', 1)
    paginator = Paginator(product, ITEMS_PER_PAGE)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    context = {'product':data,'search_key':search_key}
    
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/search.html',context)
    else:
        return render(request,'shop/web/search.html',context)

@login_required
def mybucket(request):
    buck = Bucket.objects.filter(buyer__account__user=request.user,is_use=True)
    cart = []
    for i in buck:
        cart.append({
            'id':i.pk,
            'name':i.product.product.name,
            'image':i.product.product.photo.first().file.url,
            'amount':i.amount,
            'price':i.product.product.price,
        })
    return JsonResponse(cart,safe=False)

@login_required
def del_bucket(request,pk):
    buck = Bucket.objects.get(pk=pk)
    buck.is_use = False
    buck.save()
    return HttpResponse(status=200)

@login_required
def shoppinginfo(request):
    bucket = Bucket.objects.filter(buyer__account__user=request.user,is_use=True)
    if not bucket.exists():
        return redirect('shop:mycart')
    try:
        address = Address.objects.filter(account__user=request.user,is_use=True)[0]
    except Exception as e:
        address = None
    creadit_card = CreaditCard.objects.filter(account__user=request.user)
    logistic = Logistic.objects.all()
    context = {
        'address':address,
        'logistic':logistic,
        'creadit_card':creadit_card,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/shoppingInfo.html',context)
    else:
        return render(request,'shop/web/Shippinginformation.html',context)

@login_required
def confirmshipping(request):
    bucket = Bucket.objects.filter(buyer__account__user=request.user,is_use=True)
    if not bucket.exists():
        return redirect('shop:mycart')
    if request.method == 'POST':
        data = request.POST
        if 'confirm' in data:
            bucket = Bucket.objects.filter(buyer__account__user=request.user,is_use=True)
            for item in bucket:
                logistic = Logistic.objects.get(pk=data['logistic'])
                order = Transaction.objects.create(
                    product = Detail.objects.get(pk=item.product.pk),
                    buyer = Buyer.objects.get(account__user=request.user),
                    amount = item.amount,
                    price = (item.product.product.price * item.amount)+ logistic.price,
                    payment_method = CreaditCard.objects.get(pk=data['payment_method']),
                    logistic_company = logistic,
                    tracking_id = ("TH20200423%05d"%(random.randint(10,99999))),
                    # tracking_id = "",
                    status = 'pa',
                    address = Address.objects.filter(account__user=request.user,is_use=True)[0]
                )
                item.is_use = False
                item.save()
                product = Product.objects.get(pk=item.product.product.pk)
                product.total_amount -= item.amount
                product.save()
            # return redirect('shop:tracking')
            return redirect('/tracking/{}'.format(order.pk))
        address = Address.objects.filter(account__user=request.user,is_use=True)[0]
        delivery_method = Logistic.objects.get(pk=data['delivery'])
        if 'card' in data['pay']:
            try:
                payment = CreaditCard.objects.get(pk=data['subpay'])
            except Exception as e:
                payment = CreaditCard.objects.filter(hold_name=data['pay'])[0]
        else:
            print(data)
            payment = CreaditCard.objects.filter(hold_name=data['pay'])[0]
        bucket = Bucket.objects.filter(buyer__account__user=request.user,is_use=True)
        order_price = 0
        for item in bucket:
            order_price+= item.amount * item.product.product.price
        context = {
            'address':address,
            'delivery_method':delivery_method,
            'payment':payment,
            'bucket':bucket,
            'order_price':order_price,
            'total_price':order_price+delivery_method.price,
        }
        if request.user_agent.is_mobile:
            return render(request,'shop/mobile/confirmshipping.html',context)
        else:
            return render(request,'shop/web/Confirmshipping.html',context)
    else:
        return HttpResponse('Method not allow',status=402)

@login_required
def editaddress(request):
    profile = Account.objects.get(user=request.user)
    if request.method == 'POST':
        data = request.POST
        if 'save' in data:
            return redirect('shop:shoppinginfo')
        use  = False
        if 'seva_as_default' in data:
            if data['seva_as_default'] == 'on':
                use = True
                for x in Address.objects.filter(account=profile):
                    x.is_use = False
                    x.save()
            Address.objects.all().is_use = False
        Address.objects.create(
            name    = data['name'],
            phone   = data['phone'],
            address1 = data['address'],
            address2 = Districts.objects.get(pk=int(data['district'])),
            account = Account.objects.get(user=request.user),
            is_use  = use,
        )
    address = Address.objects.filter(account=profile).order_by('-is_use')
    context = {'profile':profile,'address':address}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/editAddress.html',context)
    else:
        return render(request,'shop/web/EditAddress.html',context)

@login_required
def tracking(request,pk):
    try:
        order = Transaction.objects.get(pk=pk,buyer__account__user=request.user)
    except Exception as e:
        msg = "can not allow tracking {}".format(e)
        return HttpResponse(msg)
    context = {'order':order}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/tracking.html',context)
    else:
        return render(request,'shop/web/Tracking.html',context)

@login_required
def seller(request):
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/seller3.html')
    else:
        return render(request,'shop/web/Mycart.html')

@login_required
def sellverify(request):
    if request.method == "POST":
        data = request.POST
        Seller.objects.create(
            account = Account.objects.get(user=request.user),
            name = data['name'],
            shop_name = data['shop_name'],
            citizen_id = data['citizen'],
            bank = Bank.objects.get(pk=data['bank']),
            account_name = data['acc_name'],
            account_number = data['acc_number'],
            total_income = 0,
            is_verify = False
        )
    wait_verify = False
    seller = Seller.objects.filter(account__user=request.user)
    if seller.exists():
        seller = seller[0]
        if seller.is_verify == False:
            wait_verify = True
        else:
            return redirect('shop:sellhome')
    banks = Bank.objects.all()
    context = {'banks':banks,'wait_verify':wait_verify}
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/seller3.html',context)
    else:
        return render(request,'shop/web/SellVerify.html',context)

@login_required
def sellhome(request):
    seller = Seller.objects.filter(account__user=request.user)
    if not seller.exists():
        return redirect('shop:sellverify')
    else:
        seller = seller[0]
        if(seller.is_verify==False):
            return redirect('shop:sellverify')
    sold_of_shop = Transaction.objects.filter(product__product__seller=seller).aggregate(Count('pk'))
    product = Product.objects.filter(seller=seller,is_use=True)
    product_of_shop = product.aggregate(Count('pk'))
    ## wait for rewiew rating
    review = Review.objects.filter(product__seller=seller)
    rating = review.aggregate(Avg('star'))
    try:
        rating = "%2.2f"%rating['star__avg']
    except Exception as e:
        print(e)
        rating = 0
    print(rating)
    context = {'seller':seller,
                'product_of_shop':product_of_shop['pk__count'],
                'sold_of_shop':sold_of_shop['pk__count'],
                'rating': rating,
                'reviews':review,
                'products':product.order_by('-pk')[:ITEMS_PER_PAGE],
                'products_mobile':product.order_by('-pk')
            }
    print(context)
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/seller-pikpay.html',context)
    else:
        return render(request,'shop/web/SellHome.html',context)

@login_required
def mysell(request):
    seller = Seller.objects.get(account__user=request.user)
    order = Transaction.objects
    sell_unconfirm =order.filter(product__product__seller=seller,status='pa').order_by('timestamp')
    sell_confirm = order.filter(product__product__seller=seller,status='sc').order_by('timestamp')
    ok_status = ['cf','re','ss']
    sell_sucess = order.filter(product__product__seller=seller,status__in=ok_status).order_by('timestamp')
    sell_cancle = order.filter(product__product__seller=seller,status='cc').order_by('timestamp')
    context = {
        'sell_unconfirm':sell_unconfirm,
        'sell_confirm':sell_confirm,
        'sell_sucess':sell_sucess,
        'sell_cancle':sell_cancle,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/mysales.html',context)
    else:
        return render(request,'shop/web/Sell_mysell.html',context)

@login_required
def seller_confirm(request,pk):
    transaction = Transaction.objects.get(pk=pk)
    transaction.status = "sc"
    transaction.save()
    return HttpResponse(status=201)

@login_required
def sellerorder(request,pk):
    order = Transaction.objects.get(pk=pk)
    if request.method == "POST":
        data = request.POST
        order.tracking_id = data['tracking_id']
        order.status = 'se'
        order.save()
        return redirect('shop:mysell')
    shipment_status = ['se','cf','re','ss']
    context = {
        'order':order,
        'send':order.status in shipment_status, 
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/seller_order.html',context)
    else:
        return render(request,'shop/web/Sell_order.html',context)

@login_required
def myproduct(request):
    product = Product.objects
    product_all         = product.filter(seller__account__user=request.user,is_use=True)
    product_in_stock    = product.filter(seller__account__user=request.user,total_amount__gt=0,is_use=True)
    product_out_stock   = product.filter(seller__account__user=request.user,total_amount=0,is_use=True)
    count_product_all   = product_all.aggregate(Count('pk'))
    count_product_in_stock = product_in_stock.aggregate(Count('pk'))
    count_product_out_stock = product_out_stock.aggregate(Count('pk'))
    context = {'product_all':product_all,
                'product_in_stock':product_in_stock,
                'product_out_stock':product_out_stock,
                'count_product_all':count_product_all['pk__count'],
                'count_product_in_stock':count_product_in_stock['pk__count'],
                'count_product_out_stock':count_product_out_stock['pk__count'],
    }
    if request.user_agent.is_mobile:
        # return render(request,'shop/mobile/Sell_myproduct.html',context)
        return redirect('shop:sellhome')
    else:
        return render(request,'shop/web/Sell_myproduct.html',context)


@login_required
def sellwallet(request):
    seller = Seller.objects.get(account__user=request.user)
    transaction = Transaction.objects
    release = transaction.filter(product__product__seller=seller,status='cf').order_by('-pk')[:ITEMS_PER_PAGE]
    wait_release = transaction.filter(product__product__seller=seller,status='se').order_by('-pk')[:ITEMS_PER_PAGE]
    context = {'seller':seller,
                'transaction':release,
                'release':release,
                'wait_release':wait_release,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/mywallet.html',context)
    else:
        return render(request,'shop/web/Sell_wallet.html',context)

@login_required        
def addproduct(request):
    if request.method == 'POST':
        data = request.POST
        print(data)
        product = Product.objects.create(
            seller = Seller.objects.get(account__user=request.user),
            name = data['product_name'],
            description = data['product_detail'],
            price = int(data['price']),
            category = Category.objects.get(pk=data['category']),
            sales = 0,
            logistic = Logistic.objects.get(pk=data['shipping']),
            total_amount = int(data['stock']),
            is_use = True
        )
        try:
            pic = [int(pic) for pic in data['picture'].split(',')]
        except Exception as e:
            print(e)
        else:
            for p in pic:
                product.photo.add(p)
        if 'model' in data:
            if len(data['model'])>0:
                model = data['model'].split('^')
                model.pop()
                for d in model:
                    detail = Detail.objects.create(
                        product = product,
                        model = d,
                        amount = int(data['stock']),
                        width = 5,
                        heigth = 5,
                        depth = 5,
                        weigth = 5,
                        is_use = True
                    )
        if 'flashsale' in data:
            if data['flashsale'] == 'yes':
                str_date = "{}-{}-{}".format(data['year'],data['month'],data['day'])
                date = datetime.datetime.strptime(str_date,"%Y-%B-%d").date()
                flash_sale = FlashSale.objects.create(
                    product = product,
                    date = date,
                    discount = int(data['percent_discount']),
                    is_flashsale = True
                )
        return redirect('shop:myproduct')
    category = Category.objects.all()
    logistic = Logistic.objects.all()
    now_year = datetime.datetime.now().year
    context = {
        'category':category,
        'day':[i+1 for i in range(31)],
        'month':[(i,calendar.month_name[i]) for i in range(1,13)],
        'year':[i for i in range(now_year,now_year+5)],
        'logistic':logistic,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/addproduct.html',context)
    else:
        return render(request,'shop/web/Sell_addproduct.html',context)

@login_required
@csrf_exempt
def upload_picture(request):
    if request.method == 'POST':
        file = request.FILES
        pic = Picture.objects.create(
            file = file['files[]']
        )
        return HttpResponse(pic.pk,status=201)
    return HttpResponse(status=404)

@login_required
def edit_product(request,pk):
    product = Product.objects.get(pk=pk)
    try:
        detail = Detail.objects.filter(product=product)
    except Detail.DoesNotExist:
        print('Detail.DoesNotExist')
        detail = []
    flash_sale = FlashSale.objects.filter(product=product)
    category = Category.objects.all()
    logistic = Logistic.objects.all()
    if request.method == 'POST':
        data = request.POST
        print(data)
        product.name = data['product_name']
        product.description = data['product_detail']
        product.price = int(data['price'])
        product.category = Category.objects.get(pk=data['category'])
        product.logistic = Logistic.objects.get(pk=data['shipping'])
        product.total_amount = data['stock']
        product.save()
        if detail.exists():
            detail = detail[0]
            detail.amount = int(data['stock'])
            detail.model = data['model']
            detail.save()
        else:
            detail = Detail.objects.create(
            product = product,
            model = data['model'],
            amount = int(data['stock']),
            width = 5,
            heigth = 5,
            depth = 5,
            weigth = 5,
            is_use = True
            )
        if 'flashsale' in data:
            if data['flashsale'] == 'yes':
                if flash_sale.exists():
                    flash_sale = flash_sale[0]
                    str_date = "{}-{}-{}".format(data['year'],data['month'],data['day'])
                    date = datetime.datetime.strptime(str_date,"%Y-%B-%d").date()
                    flash_sale.date = date
                    flash_sale.discount = int(data['percent_discount'])
                    flash_sale.save()
                    print('change Date')
                else:
                    str_date = "{}-{}-{}".format(data['year'],data['month'],data['day'])
                    date = datetime.datetime.strptime(str_date,"%Y-%B-%d").date()
                    flash_sale = FlashSale.objects.create(
                        product = product,
                        date = date,
                        discount = int(data['percent_discount']),
                        is_flashsale = True
                    )
        return redirect('shop:myproduct')
    if detail.exists():
        detail = detail[0]
    if flash_sale.exists():
        flash_sale = flash_sale[0]
    now_year = datetime.datetime.now().year
    context = {
        'product':product,
        'category':category,
        'day':[i+1 for i in range(31)],
        'month':[(i,calendar.month_name[i]) for i in range(1,13)],
        'year':[i for i in range(now_year,now_year+5)],
        'logistic':logistic,
        'detail':detail,
        'flash_sale':flash_sale,
    }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/editproduct2.html',context)
    else:
        return render(request,'shop/web/Sell_editproduct.html',context)

@login_required
def cancle_order(request,pk):
    try:
        product = Transaction.objects.filter(pk=pk,buyer__account__user=request.user)[0]
    except Exception as e:
        return HttpResponse('Do not cancle other order')
    else:
        product.status = 'cc'
        product.save()
    return redirect('shop:order')

@login_required
def confirm_order(request,pk):
    try:
        order = Transaction.objects.get(pk=pk,buyer__account__user=request.user)
    except Exception as e:
        return HttpResponse('Do not confirm other order{}'.format(e))
    else:
        seller = Seller.objects.get(pk=order.product.product.seller.pk)
        seller.total_income += order.price
        seller.save()
        order.status = 're'
        order.save()
        return HttpResponse("ok",status=201)

def shop_product(request,pk):
    seller = Seller.objects.get(pk=pk)
    if not seller:
        return redirect('shop:home')
    sold_of_shop = Transaction.objects.filter(product__product__seller=seller).aggregate(Count('pk'))
    product = Product.objects.filter(seller=seller,is_use=True)
    product_of_shop = product.aggregate(Count('pk'))
    ## wait for rewiew rating
    review = Review.objects.filter(product__seller=seller)
    rating = review.aggregate(Avg('star'))
    print(rating)
    product = Product.objects
    product_all         = product.filter(seller=seller,is_use=True)
    product_in_stock    = product.filter(seller=seller,total_amount__gt=0,is_use=True)
    product_out_stock   = product.filter(seller=seller,total_amount=0,is_use=True)
    count_product_all   = product_all.aggregate(Count('pk'))
    count_product_in_stock = product_in_stock.aggregate(Count('pk'))
    count_product_out_stock = product_out_stock.aggregate(Count('pk'))
    context = {'seller':seller,
                'product_of_shop':product_of_shop['pk__count'],
                'sold_of_shop':sold_of_shop['pk__count'],
                'rating':"%2.2f"%rating['star__avg'],
                'reviews':review,
                'products':product.order_by('-pk')[:ITEMS_PER_PAGE],
                'product_all':product_all,
                'product_in_stock':product_in_stock,
                'product_out_stock':product_out_stock,
                'count_product_all':count_product_all['pk__count'],
                'count_product_in_stock':count_product_in_stock['pk__count'],
                'count_product_out_stock':count_product_out_stock['pk__count'],
            }
    if request.user_agent.is_mobile:
        return render(request,'shop/mobile/editproduct2.html',context)
    else:
        return render(request,'shop/web/shop_product.html',context)

@login_required
def del_product(request,pk):
    product = Product.objects.get(pk=pk)
    product.is_use = False
    product.save()
    return HttpResponse('Delete',status=203)