from django.db import models
from django.contrib.auth.signals import user_logged_in
from authen.models import Account,Address
import re

# Create your models here.

class Bank(models.Model):
    short_en = models.CharField(max_length=5)
    name = models.CharField(max_length=100)

    def __str__(self):
        return "{}-{}".format(self.short_en,self.name)
    

class Seller(models.Model):
    account = models.OneToOneField(Account,on_delete=models.SET_NULL,null=True)
    name = models.CharField(max_length=200)
    shop_name = models.CharField(max_length=100)
    citizen_id = models.CharField(max_length=13)
    bank = models.ForeignKey(Bank,on_delete=models.SET_NULL,null=True)
    account_name = models.CharField(max_length=200)
    account_number = models.CharField(max_length=20)
    total_income = models.PositiveIntegerField()
    is_verify = models.BooleanField()
    def __str__(self):
        return self.shop_name

class Buyer(models.Model):
    account = models.OneToOneField(Account,on_delete=models.SET_NULL,null=True)
    total_buyer = models.PositiveIntegerField()
    def __str__(self):
        return self.account.user.username

class Tags(models.Model):
    name = models.CharField(max_length=100)
    types = models.CharField(max_length=100)
    def __str__(self):
        return "{}-{}".format(self.name,self.types)

class Category(models.Model):
    name = models.CharField(max_length=100)
    def __str__(self):
        return self.name

class Picture(models.Model):
    file = models.ImageField()
    def __str__(self):
        return self.file.name
        
class Logistic(models.Model):
    name = models.CharField(max_length=100)
    detail = models.TextField()
    price = models.PositiveIntegerField()

    def __str__(self):
        return self.name

class Product(models.Model):
    seller = models.ForeignKey(Seller,on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    description = models.TextField()
    price = models.PositiveIntegerField()
    photo = models.ManyToManyField(Picture)
    category = models.ForeignKey(Category,on_delete=models.SET_NULL,null=True)
    tag = models.ManyToManyField(Tags)
    sales = models.PositiveIntegerField()
    logistic = models.ForeignKey(Logistic,on_delete=models.SET_NULL,null=True)
    total_amount = models.PositiveIntegerField()
    is_use = models.BooleanField(default=True)
    
    def __str__(self):
        return "{} {}-{}".format(self.pk,self.name,self.seller)

class Detail(models.Model):
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    model = models.CharField(max_length=100)
    amount = models.PositiveSmallIntegerField()
    width = models.PositiveSmallIntegerField()
    heigth = models.PositiveSmallIntegerField()
    depth = models.PositiveSmallIntegerField()
    weigth = models.PositiveSmallIntegerField()
    is_use = models.BooleanField(default=True)

    def __str__(self):
        return "{}-{}".format(self.product.name,self.model)

class CreaditCard(models.Model):
    account = models.ForeignKey(Account,on_delete=models.CASCADE)
    card_number = models.CharField(max_length=20)
    hold_name = models.CharField(max_length=200)
    expire = models.DateField()
    secure = models.CharField(max_length=10)
    KIND_CHOICES = [
        ('en','electron'),
        ('mo','maestro'),
        ('dt','dankort'),
        ('it','interpayment'),
        ('uy','unionpay'),
        ('va','visa'),
        ('md','mastercard'),
        ('ax','amex'),
        ('ds','diners'),
        ('dr','discover'),
        ('jb','jcb'),
        ('nk','unknown'),
        ('ba','bank'),
        ('cd','cod'),
    ]
    kind = models.CharField(max_length=2,choices=KIND_CHOICES,default='nk')

    def creadit_card_type(creaditcard_no):
        patterns={'mo': '^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$',
                'en': '^(4026|417500|4405|4508|4844|4913|4917)\d+$',
                'dt': '^(5019)\d+$',
                'it': '^(636)\d+$',
                'uy': '^(62|88)\d+$/',
                'va': '^4[0-9]{12}(?:[0-9]{3})?$',
                'md': '^5[1-5][0-9]{14}$',
                'ax': '^3[47][0-9]{13}$',
                'ds': '^3(?:0[0-5]|[68][0-9])[0-9]{11}$',
                'dr': '^6(?:011|5[0-9]{2})[0-9]{12}$',
                'jb': '^(?:2131|1800|35\d{3})\d{11}$'
        }
        for pattern in patterns:
            if re.match(patterns[pattern],creaditcard_no):
                return pattern
            else:
                return 'uk'
    

class Transaction(models.Model):
    product = models.ForeignKey(Detail,on_delete=models.SET_NULL,null=True)
    buyer = models.ForeignKey(Buyer,on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField()
    price = models.PositiveIntegerField()
    payment_method = models.ForeignKey(CreaditCard,on_delete=models.SET_NULL,null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    logistic_company = models.ForeignKey(Logistic,on_delete=models.SET_NULL,null=True)
    tracking_id = models.CharField(max_length=20)
    address = models.ForeignKey(Address,on_delete=models.SET_NULL,null=True)
    STATUS_CHOICES = [
        ('no','new order'),
        ('pa','paid'),
        ('sc','seller confirm'),
        ('se','sent'),
        ('cf','customer confirm'),
        ('re','waiting review'),
        ('ss','sucess'),
        ('cc','cancle'),
    ]
    status = models.CharField(max_length=2,choices=STATUS_CHOICES,default='no')

    # def __str__(self):
    #     return

class Bucket(models.Model):
    buyer = models.ForeignKey(Buyer,on_delete=models.CASCADE)
    product = models.ForeignKey(Detail,on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField()
    is_use = models.BooleanField(default=True)

class Review(models.Model):
    transaction = models.ForeignKey(Transaction,on_delete=models.SET_NULL,null=True)
    buyer = models.ForeignKey(Buyer,on_delete=models.SET_NULL,null=True)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    star = models.PositiveSmallIntegerField()
    description = models.TextField(null=True,blank=True)
    photo = models.ManyToManyField(Picture,blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)

class TitleReport(models.Model):
    title = models.CharField(max_length=200)
    KIND_CHOICES = [
        ('y','pikpay'),
        ('p','product'),
        ('s','seller'),
        ('b','buyer')
    ]
    kind = models.CharField(max_length=1,choices=KIND_CHOICES,default='y')

    def __str__(self):
        return "{}-{}".format(self.kind,self.title)

class Report(models.Model):
    user = models.ForeignKey(Account,on_delete=models.SET_NULL,null=True)
    product = models.ForeignKey(Product,on_delete=models.SET_NULL,null=True)
    title = models.ForeignKey(TitleReport,on_delete=models.SET_NULL,null=True)
    comment = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

class Favorite(models.Model):
    user = models.ForeignKey(Buyer,on_delete=models.SET_NULL,null=True)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    is_use = models.BooleanField(default=False)

class FlashSale(models.Model):
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    date = models.DateField()
    discount = models.PositiveSmallIntegerField()
    is_flashsale = models.BooleanField()

def logged_in_handle(sender, user, request, **kwargs):
    account = Account.objects.get(user=request.user)
    buyer = Buyer.objects.filter(account=account)
    if not buyer.exists():
        Buyer.objects.create(
            account = account,
            total_buyer = 0
        )
user_logged_in.connect(logged_in_handle)