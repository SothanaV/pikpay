from django import template

register = template.Library()


@register.simple_tag()
def percentage(price, percent, *args, **kwargs):
    return int(price*(1-(percent/100)))