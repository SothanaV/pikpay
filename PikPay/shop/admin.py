from django.contrib import admin
from .models import Seller,Buyer,Tags,Category,Product,Detail, \
                    Logistic,Transaction,Bucket,Picture,CreaditCard,\
                    Favorite,FlashSale,Bank,Review
# Register your models here.
class SellerAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Seller._meta.fields]
    list_editable=['is_verify']
admin.site.register(Seller,SellerAdmin)

class BuyerAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Buyer._meta.fields]
    # list_editable=[]
admin.site.register(Buyer,BuyerAdmin)

class TagsAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Tags._meta.fields]
admin.site.register(Tags,TagsAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Category._meta.fields]
admin.site.register(Category,CategoryAdmin)

class ProductAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Product._meta.fields]
    list_filter = ['category',]
    list_editable = ['category','is_use']
admin.site.register(Product,ProductAdmin)

class DetailAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Detail._meta.fields]
admin.site.register(Detail,DetailAdmin)

class LogisticAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Logistic._meta.fields]
admin.site.register(Logistic,LogisticAdmin)

class TransactionAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Transaction._meta.fields]
    list_editable = ['status']
admin.site.register(Transaction,TransactionAdmin)

class BucketnAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Bucket._meta.fields]
    list_editable = ['is_use']
admin.site.register(Bucket,BucketnAdmin)

class PictureAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Picture._meta.fields]
admin.site.register(Picture)

class CreaditCardAdmin(admin.ModelAdmin):
    list_display=[f.name for f in CreaditCard._meta.fields]
admin.site.register(CreaditCard,CreaditCardAdmin)

class FavoriteAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Favorite._meta.fields]
admin.site.register(Favorite,FavoriteAdmin)

class FlashSaleAdmin(admin.ModelAdmin):
    list_display=[f.name for f in FlashSale._meta.fields]
    list_editable = ['date']
admin.site.register(FlashSale,FlashSaleAdmin)

class BankAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Bank._meta.fields]
admin.site.register(Bank,BankAdmin)

class ReviewAdmin(admin.ModelAdmin):
    list_display=[f.name for f in Review._meta.fields]
admin.site.register(Review,ReviewAdmin)