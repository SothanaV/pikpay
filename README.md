# PikPay
## What is PikPay?
  PikPay is online shopping web platform

#
## How to install PikPay
 - requirement
   - docker >= 19.03.8
   - docker-compose >= 1.25.4
 - recommend
   - run on unix base (eg.macOS, Unbuntu)

### Prepare .env 
    DJANGO_SECRET=<SecretKey>
    DB_HOST=db
    DB_PORT=5432

    POSTGRES_DB=pikpay
    POSTGRES_USER=pikpay
    POSTGRES_PASSWORD=<Database Password>

    SOCIAL_AUTH_FACEBOOK_KEY=<Facebook auth Key>
    SOCIAL_AUTH_FACEBOOK_SECRET=<Facebook auth secret>

    GOOGLE_KEY=<Google Auth Key>
    GOOGLE_SECRET=<Google Auth Secret>

    DJANGO_ALLOW_ASYNC_UNSAFE=true
    PYTHONUNBUFFERED=1

### Install
    git clone https://gitlab.com/SothanaV/pikpay.git
    cd pikpay
    docker-compose up --build

- then waiting for success build

### prepare admin password
    docker-compose exec web bash

    ## when inside docker

    python manage.py createsuperuser

- createsuper(admin)

### prepare data 
    goto : localhost:8888 
    then open init_database.ipynb
    run all cell

    # check
    goto localhost:8000/admin
    enter username and password
    you can see table
        1 shop
        2 authen
        3 authen and user

# 
# when install build and preparedata success

    Open your browser go to http://localhost:8000
    Enjoy to shopping platform